# Testing simlation of generating random points 
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

import random
def create_random_point(x0,y0,distance):
    """
            Utility method for simulation of the points
    """   
    r = distance/ 111300
    u = np.random.uniform(0,1)
    v = np.random.uniform(0,1)
    w = r * np.sqrt(u)
    t = 2 * np.pi * v
    x = w * np.cos(t)
    x1 = x / np.cos(y0)
    y = w * np.sin(t)
    return (x0+x1, y0 +y)

fig = plt.figure()

latitude1,longitude1 = 13.04738626,77.61946793  

keywords=[
"Category","Accounting","Admin","Advertising","Agriculture","Architecture","Arts","BFSI",
"Bpo Jobs","Buy & Sell","Cars & Vehicles","Construction Jobs","Consultant Jobs","Customer Service Jobs",
"Data Science & Analytics","Design & Creative","Education Jobs","Electrical & Eletronics Jobs","Energy Jobs",
"Engineering & Architecture","Facilities Jobs","Food Service Jobs","Healthcare Jobs","Home Cleaning & Repairs",
"Hospitality Jobs","Human Resources Jobs","IT & Networking","IT & Systems Jobs","IT Jobs","Legal Jobs","Lessons & Hobbies",
"Logistics Jobs","Management Jobs","Manufacturing Jobs","Marketing Jobs","Mechanical Jobs","Medical Jobs","Networking Jobs",
"Operations Jobs","Part-time Jobs","Pets","Pharmaceutical Jobs","PR Jobs","Publishing Jobs","Real Estate Jobs",
"Restaurant Jobs","Retail Jobs","Sales & Marketing Jobs","Scientific Research Jobs","Security Jobs","Services Jobs",
"SEO & Digital Marketing Jobs","Teacher Jobs","Telecommunication Jobs","Translation","Transportation Jobs","Volunteering Jobs",
"Web, Mobile & Software Dev","Writing","Weddings & Events","other Accounting","Account Assistant jobs","Administrative Assistant jobs",
"Brand Manager jobs","Agriculture jobs","Architect jobs","Art Director jobs","Bank Accountant jobs","Bpo - Process Associates jobs",
"All in Buy & Sell","All in Cars & Vehicles","Maintenance Manager jobs","Associate Consultant jobs","Client Servicing Executive jobs",
"All Data Science & Analytics","UI/UX Designer","Academic Counselor jobs","Electrical Designer jobs","Energy Advisor jobs",
"Aeronautical Engineer jobs","Ac Technician jobs","Attendant-food & Beverage jobs","Associate Specialist jobs","ctrician",
"Banquet Manager jobs","HR Generalist Jobs","All IT & Networking","IT Project Management Jobs",".NET Architecture jobs",
"General Legal Support jobs","Bollywood Dance Classes","Inventory jobs","Agency Development Manager jobs",
"Manufacturing Engineer jobs","Direct Marketing Executive jobs","CNC Machinist jobs","Clinical Data Management jobs",
"Hardware Networking Engineer jobs","Supply Chain Jobs","Bpo Inbound Process jobs","All in Pets","Chemical Analyst jobs",
"Client Relationship Executive jobs","Assistant Editor jobs","Assistant Property Manager jobs","Assistant Restaurant Manager jobs",
"Buyer jobs","Sales Jobs","Analytical Chemistry jobs","Health and Safety Manager jobs","Beautician jobs",
"Digital Marketing Analyst jobs","Computer Teacher jobs","Communications Specialist jobs","All Translation",
"Bus Driver jobs","Community Support jobs","Mobile Developer","All Writing","Birthday Party Planner",
"Attorney Admin","Audit Manager jobs","Document Controller jobs","Campaign Manager jobs","Animal Care jobs",
"Landscape Architect jobs","Fashion Designer jobs","Credit Analyst jobs","Bpo Inbound Process jobs",
"Arts & Collectibles","Cars & Trucks","Site Supervisor jobs","Implementation Consultant jobs",
"Customer Service Executive jobs","A/B Testing","User Researcher","Education Senior Management jobs",
"Electrical Maintenance Engineer jobs","Power Supply jobs","Environmental Engineer jobs","Floor Manager jobs",
"Catering Assistant jobs","General Practice jobs","Plumber","Guest Relation Executive jobs","HR Business Partner Jobs",
"Database Administration","IT Consulting Jobs","Java/j2ee Developer jobs","Legal Assistant jobs","Home Tutor",
"Operations Internship jobs","Deputy Manager jobs","Production Planning Manager jobs","Marketing Coordinator jobs",
"Maintenance Mechanic jobs","Medical Officer jobs","Network Manager jobs","Operations Jobs","Customer Care Executive jobs",
"Animal & Pet Services","Lab Technician jobs","Customer Relationship Manager jobs","Editorial Assistant jobs",
"Land Surveyor jobs","Executive Chef jobs","Purchase Manager jobs","Account Executive jobs","Food Science jobs",
"Security Officer jobs","Home Manager jobs","Media Account Manager jobs","Physics Teacher jobs",
"Telecommunications Technician jobs","General Translation","Delivery Executive jobs","Fundraising jobs",
"Frontend Developer","Academic Writing & Research","Bridal Makeup Artist","Business Analyst Advertising",
"Financial Accountant jobs","Office Boy jobs","Director of Communication jobs","Horticulture jobs",
"Illustrator jobs","Retail Banking jobs","Bpo Voice Process jobs","Audio","Classic Cars","Construction Manager jobs",
"Senior Consultant jobs","Customer Support Executive jobs","Data Visualization","Visual Designer","Teaching Assistant jobs",
"Electrical Systems Engineer jobs","Power Engineer jobs","Project Engineer jobs","Plant Manager jobs","Cook jobs",
"Personal Training jobs","Carpenter","Hotel Management jobs","Talent Acquisition Jobs","ERP / CRM Software",
"Presales Jobs","Senior Software Developer jobs","Legal Secretary jobs","English Tutor","Warehouse Executive jobs",
"Programme Management jobs","Quality Controller jobs","Content Creator","Mechanical Engineer jobs",
"Medical Writer jobs","Network Engineer jobs","Procurement Jobs","Graphic Designer jobs","Birds for Rehoming",
"Pharmaceutical Sales Representative jobs","Public Relation Officer jobs","Technical Writer jobs","Real Estate Agent jobs",
"Server jobs","Store Executive jobs","Sales Support jobs","Process Chemistry jobs","Local Government jobs","Personal Care jobs",
"Social Media Marketing jobs","School Teacher jobs","Wireless Consultant jobs","Legal Translation","Shipping jobs",
"Red Cross jobs","Backend Developer","Article & Blog Writing","Mehendi Artists","Data Scientist","Agriculture",
"Accountant jobs","Administrative Executive jobs","Branding jobs","Creative Designer jobs","Bank Officer jobs",
"Bpo Executive jobs","Baby Items","Auto Parts & Tires","Planning Manager jobs","Business Advisor jobs","Customer Care Executive jobs",
"Data Extraction / ETL","Creative Director","Admission Counselor jobs","Electrical Engineer jobs","Resource Trading jobs",
"Application Engineer jobs","Building Services Maintenance jobs","Baker jobs","Dialysis Technician jobs","Pest Control",
"Cleaner jobs","Organization Development Jobs","Information Security","IT Sales Jobs",".NET Developer jobs",
"Head of Procurement jobs","Drum Classes","Logistics Analyst jobs","Agency Manager jobs","Packing Boys jobs",
"Direct Marketing Manager jobs","CNC Operator jobs","Gynecologist jobs","Network Security Engineer jobs",
"Inventory Management Jobs","Computer Operator jobs","Cats & Kittens for Rehoming","Chemical Engineer jobs",
"Client Relationship Manager jobs","Content Developer jobs","Estate Agent jobs","Bar Management jobs","Cashier jobs",
"Inside Sales jobs","Bioinformatics jobs","Security Consultant jobs","Commercial Driver jobs","Digital Marketing Executive jobs",
"English Teacher jobs","Telecom Software jobs","Medical Translation","Delivery Driver jobs","Full-Stack Developer","Copywriting",
"Party Makeup Artist","Product Manager Architecture","Audit Senior jobs","Executive Assistant jobs","Creative Director jobs",
"Graphic & Web Designer jobs","Fund Management jobs","Bpo Non Voice Process jobs","Bikes","Automotive Services",
"Interior Designer jobs","Management Consultant jobs","Customer Service Manager jobs","Data Mining & Management",
"Animation","Higher Education jobs","Electrical Supervisor jobs","Field Engineer jobs","Foreman jobs",
"Contract Catering Management jobs","Health Education jobs","Home Deep Cleaning","Guest Service jobs",
"Learning and Development Jobs","Network & System Administration","IT Product Management Jobs","Junior Business Analyst jobs",
"Legal Counsel jobs","French Lessons","Process Associate jobs","Design Manager jobs","Production Supervisor jobs",
"Marketing Director jobs","Mechanic jobs","Medical Representative jobs","Plant Operations Jobs","Data Entry jobs",
"Dogs & Puppies for Rehoming","Laboratory Manager jobs","Customer Relationship Officer jobs","Journalism jobs",
"Property Manager jobs","Kitchen Manager jobs","Purchase Officer jobs","Salesman jobs","Materials Science jobs",
"Security Supervisor jobs","Housekeeping jobs","Social Media Analyst jobs","Post Graduate Teacher jobs","Telecoms Consultant jobs",
"Technical Translation","Driver jobs","Engineering Manager","Creative Writing","Pre-Wedding Shoot","Project Manage Arts",
"Internal Auditor jobs","Office Coordinator jobs","Events Design jobs","Photographer jobs","Settlements jobs",
"Call Center Executive jobs","Books","Motorcycles","Quantity Surveyor jobs","Service Advisor jobs",
"Customer Support Representative jobs","Machine Learning","Audio Production","Teaching Professional jobs",
"Electrical Technician jobs","Quality Control Engineer jobs","Service Technician jobs","Private Practice jobs",
"Bathroom Deep Cleaning","Hotel Receptionist jobs","Compensation & Benefits Jobs","Other - IT & Networking",
"IT Business Analyst Jobs","Senior Software Engineer jobs","Litigation jobs","German Lessons","Warehouse Lead jobs",
"Project Lead jobs","Quality Engineer jobs","Online Marketing jobs","Mechanical Fitter jobs","Select State",
"Demand Planning Jobs","Marketing Executive jobs","Equestrian & Livestock Accessories","Pharmacist jobs",
"Public Relations Manager jobs","Video Editor jobs","Housing Officer jobs","Sous Chef jobs","Store Incharge jobs",
"Area Sales Executive jobs","Research jobs","Public Service jobs","Personal Driver jobs","Social Media Marketing Internship jobs",
"Secondary Teacher jobs","Telecommunications Specialist jobs","Transportation Manager jobs","QA & Testing","Editing & Proofreading",
"Event Photographer","BFSI","Accounting Manager jobs","Administrative Officer jobs","Creative Graphic Designer jobs","Banking jobs",
"BPO Operations Jobs","Business & Industrial","ATVs & Snowmobiles","Change Management jobs","Customer Care Representative jobs",
"Quantitative Analysis","Graphic Design","Assistant Professor jobs","Electronics Design Engineer jobs",
"Application Support Engineer jobs","Drilling jobs","Dietician jobs","Sofa Cleaning","Event Manager jobs",
"3d Animator jobs","Lawyer jobs","Guitar Classes","Logistics Coordinator jobs","Area Business Manager jobs",
"Product Design jobs","Market Research Analyst jobs","Drafter jobs","Physiotherapist jobs","Content Writer jobs",
"Fish for Rehoming","Chemist jobs","Customer Account Manager jobs","Content Editor jobs","All in Real Estate",
"Bartender jobs","Commercial Enterprise jobs","Inside Sales Executive jobs","Biotechnology jobs","Security Contracts Manager jobs",
"Electrician jobs","Digital Marketing Internship jobs","Hindi Teacher jobs","Telecoms Engineer jobs","DevOps","Grant Writing",
"Wedding Planner","Bpo Jobs","Auditor jobs","Executive Secretary jobs","Graphic Artist jobs","Investment Analyst jobs",
"BPO Quality Jobs","Cameras & Camcorders","Boats & Watercraft","Principal Consultant jobs","Customer Service Officer jobs",
"Other - Data Science & Analytics","Illustration","Lecturer jobs","Embedded Developer jobs","Field Service Engineer jobs",
"Iti Electrician jobs","Junior Resident jobs","Kitchen Deep Cleaning","Guest Service Representative jobs",
"Junior PHP Developer jobs","Legal Executive jobs","Guitar Lessons at Home","Process Executive jobs","Development Manager jobs",
"QA Analyst jobs","Marketing Executive jobs","Mechanical Design Engineer jobs","Medical Advisor jobs","Delivery Boy jobs",
"Horses & Ponies for Rehoming","Laboratory Technician jobs","PR Account Manager jobs","Journalist jobs",
"Apartments & Condos for Rent","Kitchen Staff jobs","Purchasing Support jobs","Senior Sales Executive jobs",
"Medical Devices jobs","Safety Officer jobs","Housekeeping Executive jobs","Social Media Executive jobs",
"Pre Primary Teacher jobs","Software Architect","Resumes & Cover Letters","Astrologer","Buy & Sell","Junior Accountant jobs",
"Office Executive jobs","Photoshop Designer jobs","Trade Support jobs","BPO Presales Jobs","CDs, DVDs & Blu-ray ",
"RVs, Campers & Trailers","Strategy Consultant jobs","Service Manager jobs","Logo Design & Branding","Visa Counselor jobs",
"Hardware & Network Engineer jobs","Senior Engineer jobs","Supervisor Maintenance jobs","Public Health jobs",
"Carpet Cleaning","Room Attendant jobs","Senior Test Engineer jobs","Legal Jobs","Keyboard Classes",
"Warehouse Manager jobs","Project Manager jobs","Quality Manager jobs","Online Marketing Executive jobs",
"Mechanical Technician jobs","Medical Transcriptionist jobs","Livestock","Pharmacology jobs","Relationship Manager jobs",
"Wordpress Developer jobs","House Rental","Supervisor-kitchen jobs","Store Keeper jobs","Area Sales Manager jobs",
"Research Scientist jobs","Security Guard jobs","Personal Secretary jobs","Social Media Specialist jobs",
"Maths Teacher jobs","Desktop Software Development","Technical Writing","Party Caterer","Cars & Vehicles",
"Accounts Assistant jobs","Administrator jobs","Designer jobs","Corporate Business Sales jobs","BPO Sales Jobs",
"Clothing","Heavy Equipment","Consultant Radiologist jobs","Customer Service Advisor jobs","Photography",
"Education Administration jobs","Electronics Engineer jobs","Automobile Engineer jobs","Facilities Assistant jobs",
"Disability Support jobs","Commercial Pest Control","Events Management jobs","Android Application Developer jobs",
"Regulatory Compliance Jobs","Keyboard Lessons at Home","Logistics Executive jobs","Area Manager jobs",
"Production jobs","Market Research Executive jobs","Machine Operator jobs","Radiologist jobs","Lost & Found",
"Clinical Research Associate jobs","Customer Advisor jobs","Content Writer jobs","Room Rentals & Roommates",
"Cabin Crew jobs","Export Executive jobs","Lead Generation Executive jobs","Data Analyst jobs","Hair Stylist jobs",
"Digital Marketing Manager jobs","Primary School Teacher jobs","Game Development","Web Content","DJ",
"Construction Jobs","Billing Coordinator jobs","Front Desk Executive jobs","Graphic Design Internship jobs",
"Reconciliations jobs","BPO Training Jobs","Computers","Other","Recruitment Consultant jobs",
"Customer Service Representative jobs","Presentations","Principal jobs","Embedded Engineer jobs",
"General Engineer jobs","Iti Fitter jobs","Medical Assistant jobs","AC Service and Repair","Guest Services Associate jobs",
"Junior Software Developer jobs","Litigation Jobs","Salsa Dance Classes","Team Leader - Operations jobs","Director jobs",
"QA Engineer jobs","Marketing Head jobs","Mechanical Designer jobs","Accessories","Pharma Sales Manager jobs","PR Executive jobs",
"News Reporter jobs","Short Term Rentals","Restaurant Manager jobs","Representative Retail Sales jobs","Senior Sales Manager jobs",
"Microbiologist jobs","Housekeeping Supervisor jobs","Social Media Internship jobs","Other - Writing","Wedding Choreographer",
"Consultant Jobs","Management Accountant jobs","Office Manager jobs","Production Artist jobs","Broker jobs","Customer Service Jobs",
"Computer Accessories","Technical Consultant jobs","Support Assistant jobs","Video Production","Education Counselor jobs",
"Service Engineer jobs","Technician jobs","Staff Nurse jobs","iPhone, iPad, Mac Repair", "Senior Web Designer jobs",
"Company Secretary Jobs","Zumba Fitness Dance","Warehouse Specialist jobs","Regional Manager jobs","Quality Service Manager jobs",
"Online Marketing Manager jobs","Procurement Manager jobs","Reptiles & Amphibians for Rehoming","Pharmacy Assistant jobs",
"Relationship Officer jobs","Writer jobs","Commercial & Office Space for Rent","Waiter jobs","Store Manager jobs",
"Area Sales Officer jobs","Scientist jobs","Social Care jobs","Social Media Strategist jobs","Wedding Caterers",
"Customer Service Jobs","Accounts Officer jobs","Assistant jobs","Fashion Consultant jobs","Finance Executive jobs",
"Electronics","Functional Consultant jobs","Customer Service Associate jobs","Voice Talent","Teacher jobs","Automotive jobs",
"Facilities Management jobs","Doctor jobs","Chimney Cleaning & Repair","Android Developer jobs",
"Intellectual Property Rights (IPR) Jobs","Logistics Manager jobs","Assistant Branch Manager jobs",
"Production Engineer jobs","Marketing Analyst jobs","Small Animals for Rehoming","Lab Assistant jobs",
"Customer Relationship Executive jobs","Copywriter jobs","Storage & Parking for Rent","Chef jobs","Night Manager jobs",
"Manager Sales jobs","Data Scientist jobs","Helper jobs","Digital Marketing Specialist jobs","Corporate Event Planner",
"Data Science & Analytics","Billing Executive jobs","Front Office Assistant jobs","Graphic Designer jobs",
"Financial Planning Manager jobs","Free Stuff","Sales Consultant jobs","Customer Support Associate jobs",
"Hardware Design Engineer jobs","Maintenance Supervisor jobs","Nurse jobs","Geyser / Water Heater Repair",
"Lead Developer jobs","Legal Jobs in BFSI","Warehouse Assistant jobs","General Management jobs","QA Lead jobs",
"Marketing Internship jobs","Other Pets for Rehoming","PR Manager jobs","Writer Editor jobs","Houses for Sale",
"Restaurant Supervisor jobs","Retail Assistant jobs","Supervisor-sales jobs","Molecular Biology jobs","Spa Therapist jobs",
"Social Media Manager jobs","Wedding Photographer","Design & Creative","Senior Accountant jobs","Operations Manager jobs",
"Senior Graphic Designer jobs","Collection Executive jobs","Furniture","Technology Consultant jobs","Site Engineer jobs",
"Welding jobs","Laptop Repair","Senior Web Developer jobs","Contract Law","Warehouse Supervisor jobs","Safety Manager jobs",
"Research Analyst jobs","Product Marketing Manager jobs","Other","Desktop Publishing jobs","Condos for Sale","Store Officer jobs",
"Assistant Sales Manager jobs","Statistician jobs","Childcare & Nanny","Internet Marketing Executive jobs",
"Pre Bridal Beauty Packages","Education Jobs","Accounts Payable jobs","Assistant Manager jobs","Finance Manager jobs",
"Garage Sales","Civil Engineer jobs","Facilities Manager jobs","Microwave Repair","Associate Software Engineer jobs",
"Corporate Law","Operations Executive jobs","Assistant Store Manager jobs","Production Executive jobs","Marketing Assistant jobs",
"Reporter jobs","Land for Sale","Online Bidder jobs","Merchandiser jobs","Environmental Science jobs",
"Fitness & Personal Trainer","SEO Analyst jobs","Bartender","Electrical & Eletronics Jobs","Chartered Accountant jobs","Front Office Associate jobs","Financial Services jobs","Health & Special Needs","Hardware Engineer jobs","Maintenance Technician jobs","Mobile Repair","Lead Software Engineer jobs","Criminal Law","Warehouse Associate jobs","General Manager jobs","QA Manager jobs","Marketing Manager jobs","Editor jobs","Real Estate Services","Retail Sales Executive jobs","Team Leader - Sales jobs","Organic Chemistry jobs","Moving & Storage","SEO Manager jobs","Wedding Florists & Decorators","Energy Jobs","Tally Operator jobs","Personal Assistant jobs","Collection Officer jobs","Hobbies & Crafts","Site Manager jobs","Refrigerator Repair","Server Virtualization jobs","Family Law","Senior Manager jobs","Textiles jobs","Marketing Associate jobs","Technical Content Writer jobs","Other","Store Staff jobs","Business Developer jobs","Music Lessons","Internet Marketing Manager jobs","Live Musician","Engineering & Architecture","Accounts Receivable jobs","Clerk jobs","Financial Advisor jobs","Home Appliances","Civil Site Engineer jobs","RO or Water Purifier Repair","C# Developer jobs","Intellectual Property Law","Branch Manager jobs","Production Manager jobs","Marketing Officer jobs","Purchase Assistant jobs","Promoter jobs","Photography & Video","SEO Executive jobs","Wedding Venues","Facilities Jobs","Company Accountant jobs","Front Office Manager jobs",
"Investments jobs","Home - Indoor","HVAC Engineer jobs","Washing Machine Repair","Linux Administrator jobs",
"Paralegal Services","Internal Communications jobs","Quality Control Manager jobs","Growth Hacker","Retail Sales Representative jobs","Technical Sales jobs","Tutors & Languages","SEO Specialist jobs","Food Service Jobs","Tax Accountant jobs","Project Coordinator jobs","Controller jobs","Home Outdoor","Structural Engineer jobs","Service Delivery jobs","Senior Project Manager jobs","Vendor Manager jobs","Content Creator","Purchase Executive jobs","Business Development Executive jobs","Marriage/ Relationship Counsellor","Internet Sales Representative jobs","Healthcare Jobs","Assistant Accountant jobs","Company Secretary jobs","Financial Analyst jobs","Home Renovation Materials","Computer Engineer jobs","C++ Developer jobs","Business Analyst jobs","Retail Store Manager jobs","Regional Sales Manager jobs","Shoe Laundry & Repair","SEO Expert jobs","Home Cleaning & Repairs","Cost Accounting jobs","Librarian jobs","Risk Management jobs","Jewellery & Watches","Industrial Engineer jobs","Linux System Administrator jobs","Knowledge Manager jobs","Technical Sales Executive jobs","Study Abroad Counsellors","SEO Trainee jobs","Hospitality Jobs","All Accounting & Consulting","Project Support jobs","Corporate Finance jobs","Musical Instruments","Support Engineer jobs","Software Designer jobs","Store Supervisor jobs","Commercial Manager jobs","CA for Income Tax Filing","Human Resources Jobs","Accounting","Contracts Manager jobs","Financial Consultant jobs","Phones","Customer Support Engineer jobs","Core Java Developer jobs","Business Development Associate jobs","Sales Associate jobs","Visa Agency","IT & Networking","Financial Planning","Library Assistant jobs","Senior Financial Analyst jobs","Sporting Goods & Exercise","Installation Engineer jobs","Magento Developer jobs","New Business Manager jobs","Telecallers jobs","Monthly Tiffin Service","IT & Systems Jobs","Human Resources","Receptionist jobs","Equity Dealer jobs","Tickets","System Analyst jobs","Software Developer jobs","Team Manager jobs","Corporate Sales Executive jobs","Fashion Photographer","IT Jobs","Management Consulting","Coordinator jobs","Financial Controller jobs","Tools","Data Engineer jobs","Crm Executive jobs","Business Development Manager jobs","Sales Coordinator jobs","Elder Care at Home","Legal Jobs","Other - Accounting & Consulting","Office Administrator jobs","Tax Manager jobs","Toys & Games","Instrumentation Engineer jobs","Mean Stack Developer jobs","Operations Management jobs","Telecalling Executive jobs","Astrologer","Lessons & Hobbies","Secretary jobs","Finance and Accounts Jobs","TVs & Video","System Engineer jobs","Software Development Manager jobs","Technical Lead jobs","Corporate Sales Manager jobs","Baby Portfolio Photographer","Logistics Jobs","Development Associate jobs","Banking Jobs","Video Games & Consoles","Design Engineer jobs","Database Administrator jobs","Business Development Officer jobs","Sales Development Manager jobs","Bag Cleaning/ Repair Service","Management Jobs","Office Assistant jobs","Corporate Banking Jobs","Other","Lead Engineer jobs","Medical Coder jobs","Practice Manager jobs","Telemarketing Executive jobs","Car Cleaning","Manufacturing Jobs","All Admin Support","Investment Banking Jobs","Test Engineer jobs","Software Engineer jobs","Technical Management jobs","Counter Sales jobs","CCTV Cameras and Installation","Marketing Jobs","Data Entry","Private Equity Jobs","Development Engineer jobs","Database Developer jobs","Business Manager jobs","Sales Executive Officer jobs","Divorce Lawyer","Mechanical Jobs","Personal / Virtual Assistant","Equity Research Jobs","Maintenance Engineer jobs","Mis Executive jobs","Product Management jobs","Telesales Executive jobs","Car Servicing & Repair","Medical Jobs","Project Management","Actuarial jobs","Trainee Engineer jobs","Software Programmer jobs","Technical Project Manager jobs","Counter Sales Executive jobs","Dry Cleaning","Networking Jobs","Transcription","Credit Risk jobs","Electrical Design Engineer jobs","Deployment Engineer jobs","Delivery Manager jobs","Sales Head jobs","Maternity Photographer","Operations Jobs","Web Research","Insurance Agent jobs","Mobile Engineer jobs","Mobile Application Developer jobs","Product Manager jobs","Territory Sales Manager jobs","Passport Agent","Part-time Jobs","Other - Admin Support","Benefits Manager jobs","Validation Engineer jobs","Software Test Engineer jobs","Territory Manager jobs","Direct Sales jobs","Salon at Home","Pets","General Insurance jobs","Engineering Manager jobs","Desktop Support Engineer jobs","Department Manager jobs","Sales Manager jobs","Spa at Home for Women","Pharmaceutical Jobs","Insurance Sales Advisor jobs","Process Engineer jobs","Mobile Application Development Internship jobs","Program Manager jobs","Vehicle Sales jobs","Party Makeup Artist","PR Jobs","Loan Consultant jobs","3D Modeling & CAD","SQL Database Administrator jobs","CFO","Direct Sales Executive jobs","Mehendi Artists","Publishing Jobs","Loan Processor jobs","Architecture","Embedded Software Engineer jobs","CMO","Sales Officer jobs","Bridal Makeup Artist","Real Estate Jobs","Loans Advisor jobs","Chemical Engineering","Network Administrator jobs","COO","Zonal Sales Manager jobs","Pre Bridal Beauty Packages","Restaurant Jobs","Loan Officer jobs","Civil & Structural Engineering","SQL Developer jobs","CTO","Field Sales jobs","Retail Jobs","Loans Administration jobs","Contract Manufacturing","Front End Developer jobs","Sales Promoter jobs","Sales Jobs","Mortgage Consultant jobs","Electrical Engineering","Oracle DBA jobs","Field Sales Executive jobs","Scientific Research Jobs","Interior Design","System Administrator jobs","Sales Representative jobs","Security Jobs","Mechanical Engineering","Full Stack Developer jobs","Field Sales Officer jobs","SEO & Digital Marketing Jobs","Product Design","Oracle Developer jobs","Sales Jobs","Services Jobs","Hardware Engineer","Technical Architect jobs","Marketing Jobs","Teacher Jobs","Mechanical Engineer","HTML Developer jobs","Brand Management Jobs","Telecommunication Jobs","Systems Engineer","Oracle PL SQL Developer jobs","Online Marketing Jobs","Translation","Technical Manager jobs","Marketing Communications Jobs","Transportation Jobs","Ios Application Developer jobs","Market Research Jobs","Volunteering Jobs","PHP Developer jobs","Business Development","Web, Mobile & Software Dev",
"Technical Support Engineer jobs","Account Manager","Weddings & Events","Ios Developer jobs","Sales Manager","Writing","PHP Web Developer jobs","Display Advertising","other","Test Lead jobs","Email & Marketing Automation","IT Executive jobs","Lead Generation","Principal Software Engineer jobs","Market & Customer Research","Tester jobs","Marketing Strategy","IT Manager jobs","Public Relations","Python Developer jobs","SEM - Search Engine Marketing","Trainee Java Developer jobs","SEO - Search Engine Optimization","IT Project Manager jobs","SMM - Social Media Marketing","Salesforce Developer jobs","Trainee Software Engineer jobs","IT Sales jobs","SAP Basis Consultant jobs","Ui/ux Designer jobs","IT System Manager jobs","Senior .NET Developer jobs","Ui/ux Developer jobs","Java Architect jobs","Senior Android Developer jobs","Unix Engineer jobs","Java Developer jobs","Senior Business Analyst jobs","User Interface Designer jobs","Java Lead jobs","Senior Developer jobs","Web Application Developer jobs","Java Programmer jobs","Senior Ios Developer jobs","Web Designer jobs","Java Script Developer jobs","Senior Java Developer jobs","Web Designer - Trainee jobs",",""Java Trainer jobs","Senior PHP Developer jobs",",""Web Developer jobs"

]



rng = random.Random(5)
counter=1
expert = False
gender = 1
for j in range(1,1000):
    print 'insert into search (id,lat,lng,category,sub_category,rating,keyword,expert,gender) VALUES'
    keyword_counter=0
    for m in range(1,1000):
        if expert ==True :
            expert = False 
        else :
            expert = True
        if gender ==1 :
            gender = 2 
        else :
            gender = 1
        rating = rng.randint(0.0,5.0)            
        x,y = create_random_point(latitude1,longitude1 ,500 )
        print '(',counter,',',x,',',y,',',j,',',m,',',rating,',"',keywords[keyword_counter],'",', expert,',', gender,'),'
        counter = counter + 1
        keyword_counter = keyword_counter + 1
    print '(',counter,',13.04738626,77.61946793,1,1,5,"test",true,1);'
    counter = counter + 1

'''

mysql> desc search;
+--------------+---------------+------+-----+---------+-------+
| Field        | Type          | Null | Key | Default | Extra |
+--------------+---------------+------+-----+---------+-------+
| id           | bigint(20)    | NO   | PRI | NULL    |       |
| name         | varchar(255)  | NO   |     | NULL    |       |
| lat          | decimal(11,8) | YES  |     | NULL    |       |
| lng          | decimal(11,8) | YES  |     | NULL    |       |
| cost         | decimal(11,3) | YES  |     | NULL    |       |
| category     | varchar(255)  | YES  |     | NULL    |       |
| sub_category | varchar(255)  | YES  |     | NULL    |       |
| active       | tinyint(1)    | NO   |     | 1       |       |
| rating       | decimal(5,3)  | NO   |     | 0.000   |       |
| keyword      | varchar(255)  | YES  |     | NULL    |       |
| expert       | tinyint(1)    | YES  |     | 0       |       |
| gender       | tinyint(1)    | NO   |     | 1       |       |
+--------------+---------------+------+-----+---------+-------+

CREATE TABLE `search` (
  `id` bigint(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lat` decimal(11,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL,
  `cost` decimal(11,3) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `sub_category` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `rating` decimal(5,3) NOT NULL DEFAULT '0.000',
  `keyword` varchar(255) DEFAULT NULL,
  `expert` tinyint(1) DEFAULT '0',
  `gender` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


set @orig_lat = 13.04738626;
set @orig_lon = 77.61946793  ;

set @mylat = 13.04738626;
set @mylon = 77.61946793  ;
set @dist = 10;

set @lon1 = @mylon-@dist/abs(cos(radians(@mylat))*69);
set @lon2 = @mylon+@dist/abs(cos(radians(@mylat))*69);
set @lat1 = @mylat-(@dist/69);
set @lat2 = @mylat+(@dist/69);

SELECT destination.*, 3956 * 2 * ASIN(SQRT( POWER(SIN((@orig_lat -lat) *  pi()/180 / 2), 2) +    
 COS(lat * pi()/180) * COS(lat * pi()/180) * POWER(SIN((@orig_lon -lng) * pi()/180 / 2), 2) )) 
 as distance FROM search destination 
 WHERE  destination.lng between @lon1 and @lon2 
 and destination.lat between @lat1 and @lat2  
 and keyword like '%car%' and gender=2 
 and expert=true 
 having distance < @dist ORDER BY Distance, cost limit 10;

'''